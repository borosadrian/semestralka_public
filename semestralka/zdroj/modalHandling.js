$(document).ready(function () {
    $('.editbtn').on('click', function() {
        $('#editmodal').modal('show');

        $tr = $(this).closest('tr');

        let data = $tr.children("td").map(function() {
            return $(this).text();
        }).get();

        console.log(data);

        $('#update_id').val(data[0]);
        $('#nazov').val(data[1]);
        $('#hodnotenie').val(data[2]);
        $('#kontakt').val(data[3]);
        $('#psc').val(data[4]);
        $('#ulica').val(data[5]);
        $('#mesto').val(data[6]);
        $('#minuty').val(data[7]);

    });
});

function showModal()
{
    if(document.readyState === 'ready' || document.readyState === 'complete') {
        document.getElementById("addEscapeButtonModal").click();
        console.log("Ready")
    } else {
        document.onreadystatechange = function () {
            if (document.readyState == "complete") {
                document.getElementById("addEscapeButtonModal").click();
                console.log("Just now");
            }
        }
    }
}
<!DOCTYPE html>
<html lang="sk">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
    <title>Escape rooms</title>

    <script>
        function myFunction() {
            var x = document.getElementById("navigacia");
            if (x.className === "menu") {
                x.className += " responsive";
            } else {
                x.className = "menu";
            }
        }
    </script>
    <link rel="stylesheet" href="semestralka/css/semestralkaCSS.css">


</head>

<body>
<div class="bg">
    <div class="menu" id="navigacia">
        <a href="index.php" class="active">Úvod</a>
        <a href="informacie.php">Informácie</a>
        <a href="kontakt.html">Kontakt</a>
        <a href="javascript:void(0);" class="icon" onclick="myFunction()">
            <div class="toggle"></div>
            <div class="toggle"></div>
        </a>
    </div>

    <h1>Vitajte na stránke Escape rooms.</h1>
    <div class="uvod">
        <div class="container-sm">
            <div>
                Vyhľadávajte
            </div>
            <div>
                Skúmajte
            </div>

            <div class="mainPosledny">
                Ohodnoťte
            </div>
        </div>
    </div>

</div>
</body>
</html>

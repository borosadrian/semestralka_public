<?php

require "EscapeRoom.php";
require "DtbStorage.php";
//require "kontrolaVstupov.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"></script>
    <script
            src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"></script>
    <script src="modalHandling.js"></script>
    <script src="countUp.js"></script>


    <title>Escape rooms</title>
    <script>
        function myFunction() {
            var x = document.getElementById("navigacia");
            if (x.className === "menu") {
                x.className += " responsive";
            } else {
                x.className = "menu";
            }
        }

        document.addEventListener("DOMContentLoaded", function() {
            var elements = document.getElementsByTagName("INPUT");
            for (var i = 0; i < elements.length; i++) {
                elements[i].oninvalid = function(e) {
                    e.target.setCustomValidity("");
                    if (!e.target.validity.valid) {
                        e.target.setCustomValidity("Toto pole je nutné vyplniť!");
                    }
                };
                elements[i].oninput = function(e) {
                    e.target.setCustomValidity("");
                };
            }
        })
    </script>
    <link rel="stylesheet" href="semestralka/css/semestralkaCSS.css">
</head>

<body>

<div class="menu" id="navigacia">
    <a href="index.php">Úvod</a>
    <a href="informacie.php" class="active">Informácie</a>
    <a href="kontakt.html">Kontakt</a>
    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
        <div class="toggle"></div>
        <div class="toggle"></div>
    </a>
</div>


<div class="infoBiela">
    <h1>
        Na čo slúži stránka Escape rooms?
    </h1>
    <div>
        Stránka je tvorená pre milovníkov hráčov únikových miestností na Slovensku.
    </div>
    <div>
        Nachádza sa tu databáza väčšiny slovenských "escapes".
    </div>
    <div>

    </div>
</div>

<div class="infoSeda">

    <h2>
        Unikneš zo všetkých?
    </h2>

    <div class="container-sm">
        <img class="img-fluid" src="obrazky/lock.png" alt="Obrázok zámky">
    </div>
</div>



<?php
$storage = new DtbStorage();
$storage->processData();
$storage->update();
$rooms = $storage->loadAllData();

?>
<div class="table-responsive-md">
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Názov</th>
            <th>Hodnotenie</th>
            <th>Kontakt</th>
            <th>Psč</th>
            <th>Ulica</th>
            <th>Mesto</th>
            <th>Minutáž</th>
            <th>Akcie</th>
        </tr>
        <?php
        foreach ($rooms as $room) {  ?>
            <tr>
                <td style="width: 30px"><?php echo $room->getId(); ?></td>
                <td><?php echo $room->getNazov(); ?></td>
                <td class="counter" data-target="<?php echo $room->getHodnotenie(); ?>">0</td>
                <td><?php echo $room->getKontakt(); ?></td>
                <td><?php echo $room->getPsc(); ?></td>
                <td><?php echo $room->getUlica(); ?></td>
                <td><?php echo $room->getMesto(); ?></td>
                <td class="counter" id="countUpStart" data-target="<?php echo $room->getMinutaz(); ?>">0</td>
                <td class="contact-delete">
                <div class="row">
                    <div class="col">
                        <form action='delete.php?id="<?php echo $room->getId(); ?>"' method="post">
                            <input type="hidden" name="id" value="<?php echo $room->getId(); ?>">
                            <button class="btn btn-danger" type="submit" name="submit" value="Zmazať">ZMAZAŤ</button>
                        </form>
                    </div>
                    <div class="col">
                        <button type="button" class="btn btn-success editbtn">UPRAVIŤ</button>
                    </div>
                </div>

                </td>

            </tr>
        <?php } ?>


    </table>
</div>


<!-- Button trigger modal -->
<div class="text-center">
<button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#escapeAddModal" data-backdrop="static" data-keyboard="false" id="addEscapeButtonModal">
    PRIDAŤ NOVÚ
</button>
</div>

<!-- Modal pridať-->
<div class="modal fade text-black" id="escapeAddModal" data-backdrop="static" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pridať novú escape room</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
            <div class="modal-body">

                    <div class="mb-3">
                        <label>Názov</label>
                        <input class="form-control" type="text" name="nazov" placeholder="Názov" >
                        <span class="error"><?php echo $nazovError;?></span>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Hodnotenie</label>
                        <input class="form-control" type="number" step="0.1" max="5" min="0" name="hodnotenie" placeholder="0-5">
                        <span class="error"><?php echo $hodnotenieError;?></span>
                    </div>
                    <div class="mb-3">
                        <label>Kontakt</label>
                        <input class="form-control" type="tel" name="kontakt" placeholder="+421.." required>
                        <span class="error"><?php echo $kontaktError;?></span>
                    </div>
                    <div class="mb-3">
                        <label>PSČ</label>
                        <input class="form-control" type="text" name="psc" placeholder="PSČ" required>
                        <span class="error"><?php echo $pscError;?></span>
                    </div>
                    <div class="mb-3">
                        <label>Ulica</label>
                        <input class="form-control" type="text" name="ulica" placeholder="Ulica" required>
                        <span class="error"><?php echo $ulicaError;?></span>
                    </div>
                    <div class="mb-3">
                        <label>Mesto</label>
                        <input class="form-control" type="text" name="mesto" placeholder="Mesto" required>
                        <span class="error"><?php echo $mestoError;?></span>
                    </div>
                    <div class="mb-3">
                        <label>Minutáž</label>
                        <input class="form-control" type="number" name="minutaz" id="minutaz" placeholder="Minutáž" required>
                        <span class="error"><?php echo $minutazError;?></span>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Zavrieť</button>
                <button type="submit" name="vlozitEscape" class="btn btn-warning">Uložiť</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- MODAL EDIT -->
<div class="modal fade text-black" id="editmodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upraviť escape room</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="update.php" method="post">
                <div class="modal-body">
                    <input type="hidden" name="update_id" id="update_id">
                    <div class="mb-3">
                        <label>Názov</label>
                        <input class="form-control" type="text" name="nazov" id="nazov" placeholder="Názov" required>

                    </div>
                    <div class="mb-3">
                        <label class="form-label">Hodnotenie</label>
                        <input class="form-control" type="number" step="0.1" max="5" min="0" name="hodnotenie" id="hodnotenie" placeholder="Hodnotenie">
                    </div>
                    <div class="mb-3">
                        <label>Kontakt</label>
                        <input class="form-control" type="tel" name="kontakt" id="kontakt" placeholder="+421.." required>
                    </div>
                    <div class="mb-3">
                        <label>PSČ</label>
                        <input class="form-control" type="text" name="psc" id="psc" placeholder="PSČ" required>
                    </div>
                    <div class="mb-3">
                        <label>Ulica</label>
                        <input class="form-control" type="text" name="ulica" id="ulica" placeholder="Ulica" required>
                    </div>
                    <div class="mb-3">
                        <label>Mesto</label>
                        <input class="form-control" type="text" name="mesto" id="mesto" placeholder="Mesto" required>
                    </div>
                    <div class="mb-3">
                        <label>Minutáž</label>
                        <input class="form-control" type="text" name="minutaz" id="minuty" required>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Zavrieť</button>
                    <button type="submit" name="upravitEscape" class="btn btn-warning">Uložiť</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!--
<div class="zoznamRooms">
    <div class="pridat">
        <h3 class="pridajteVlastnu">
            Pridajte svoju vlastnú Escape room
        </h3>
        <form method="post">
            <input type="text" name="nazov" placeholder="Názov" required>
            <input type="number" step="0.1" max="5" name="hodnotenie" placeholder="Hodnotenie" required>
            <input type="tel" name="kontakt" placeholder="+421.." required>
            <input type="text" name="psc" placeholder="PSČ" required>
            <input type="text" name="ulica" placeholder="Ulica" required>
            <input type="text" name="mesto" placeholder="Mesto" required>
            <input type="text" name="minutaz" id="minutaz" placeholder="Minutáž" required>
            <input type="submit" value="Pridať" name="sent">
        </form>
    </div>
-->

<!--
    <div class="zmenit">
        <h3 class="zmenteNeaktualnu">
            Zmeňte neaktuálnu
        </h3>
        <form method="post">
            <label for="stlpec">Zvoľte menenú hodnotu:</label>
            <select name="stlpec" id="stlpec">
                <option value="nazov">Názov</option>
                <option value="minutaz">Minutáž</option>
                <option value="hodnotenie">Hodnotenie</option>
                <option value="mesto">Mesto</option>
                <option value="kontakt">Kontakt</option>
            </select>
            <input type="text" name="menene" placeholder="Nová hodnota" required>
            <input type="number" min="1" name="zaznam" placeholder="ID" required>
            <input type="submit" value="zmeniť" name="zmenit">
        </form>
    </div>
    -->
</div>


</body>
</html>

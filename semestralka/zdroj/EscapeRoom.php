<?php


class EscapeRoom
{
    private $nazov;
    private $hodnotenie;
    private $minutaz;
    private $kontakt;
    private $psc;
    private $ulica;
    private $mesto;
    private $id;

    public function __construct($nazov, $hodnotenie, $kontakt, $psc, $ulica, $mesto, $minutaz, $id = 0)
    {
        $this->nazov = $nazov;
        $this->minutaz = $minutaz;
        $this->hodnotenie = $hodnotenie;
        $this->kontakt = $kontakt;
        $this->mesto = $mesto;
        $this->psc = $psc;
        $this->ulica = $ulica;
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNazov()
    {
        return $this->nazov;
    }

    /**
     * @param mixed $nazov
     */
    public function setNazov($nazov)
    {
        $this->nazov = $nazov;
    }


    /**
     * @return mixed
     */
    public function getMinutaz()
    {
        return $this->minutaz;
    }

    /**
     * @param mixed $minutaz
     */
    public function setMinutaz($minutaz)
    {
        $this->minutaz = $minutaz;
    }

    /**
     * @return mixed
     */
    public function getHodnotenie()
    {
        return $this->hodnotenie;
    }

    /**
     * @param mixed $hodnotenie
     */
    public function setHodnotenie($hodnotenie)
    {
        $this->hodnotenie = $hodnotenie;
    }

    /**
     * @return mixed
     */
    public function getKontakt()
    {
        return $this->kontakt;
    }

    /**
     * @param mixed $kontakt
     */
    public function setKontakt($kontakt): void
    {
        $this->kontakt = $kontakt;
    }

    /**
     * @return mixed
     */
    public function getPsc()
    {
        return $this->psc;
    }

    /**
     * @param mixed $psc
     */
    public function setPsc($psc): void
    {
        $this->psc = $psc;
    }

    /**
     * @return mixed
     */
    public function getUlica()
    {
        return $this->ulica;
    }

    /**
     * @param mixed $ulica
     */
    public function setUlica($ulica): void
    {
        $this->ulica = $ulica;
    }

    /**
     * @return mixed
     */
    public function getMesto()
    {
        return $this->mesto;
    }

    /**
     * @param mixed $mesto
     */
    public function setMesto($mesto): void
    {
        $this->mesto = $mesto;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }


}
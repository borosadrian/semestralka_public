<?php
include 'kontrolaVstupov.php';
class DtbStorage
{

    private const DB_HOST = 'localhost';
    private const DB_NAME = 'escaperooms';
    private const DB_USER = 'root';
    private const DB_PASS = 'dtb456';

    private $db;

    public function __construct()
    {
        try {
            $this->db = new PDO('mysql:dbname=' . self::DB_NAME . ';host=' . self::DB_HOST, self::DB_USER, self::DB_PASS);
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }

    function loadAllData()
    {
        $rooms = [];
        $dbRooms = $this->db->query('SELECT * from rooms');
        foreach ($dbRooms as $room)
        {
            $rooms[] = new EscapeRoom($room['nazov'], $room['hodnotenie'], $room['kontakt'], $room['psc'], $room['ulica'], $room['mesto'], $room['minutaz'], $room['id']);
        }
        return $rooms;
    }

    function save(EscapeRoom $escapeRoom)
    {
        $sql = 'INSERT INTO rooms(nazov, hodnotenie, kontakt, psc, ulica, mesto, minutaz) VALUES (?, ?, ?, ?, ?, ?, ?)';
        $this->db->prepare($sql)->execute([$escapeRoom->getNazov(), $escapeRoom->getHodnotenie(), $escapeRoom->getKontakt(), $escapeRoom->getPsc(),
            $escapeRoom->getUlica(), $escapeRoom->getMesto(), $escapeRoom->getMinutaz()]);
    }

    public function processData()
    {
        if (isset($_POST['vlozitEscape'])) {
            if ($GLOBALS['kod'] == "0")
            {
                $this->save(new EscapeRoom($_POST['nazov'], $_POST['hodnotenie'], $_POST['kontakt'], $_POST['psc'],
                    $_POST['ulica'], $_POST['mesto'], $_POST['minutaz']));
            } else
            {
                echo  "<script type='text/javascript'>
                $(document).ready(function() {
                    showModal();
                });               
                </script>";
            }


        }

    }


    function update() {
        if (isset($_POST['zmenit'])) {
            $idUpdate = $_POST['zaznam'];
            $stlpec = $_POST['stlpec'];
            $novaHodnota = $_POST['menene'];
            $sql = "UPDATE rooms SET $stlpec = ? WHERE id = $idUpdate";
            $update = $this->db->prepare($sql);
            $update->execute([$novaHodnota]);
            if ($update->rowCount() > 0) {
                ?>
                <script>alert("Záznam bol úspešne upravený")</script>
                <?php
            } else {
                ?>
                <script>alert("Záznam sa nepodarilo upraviť!")</script>
                <?php
            }
        }
    }
}
<?php

    $kod = "0";
    $nazovError = "";
    $hodnotenieError = "";
    $kontaktError = "";
    $pscError = "";
    $ulicaError = "";
    $mestoError = "";
    $minutazError = "";
    if (isset($_POST['vlozitEscape'])) {
        $nazovKontrola = trim($_POST["nazov"]);
        $hodnotenieKontrola = trim($_POST["hodnotenie"]);
        $kontaktKontrola = trim($_POST["kontakt"]);
        $pscKontrola = trim($_POST["psc"]);
        $ulicaKontrola = trim($_POST["ulica"]);
        $mestoKontrola = trim($_POST["mesto"]);
        $minutazKontrola = trim($_POST["minutaz"]);

        if ($nazovKontrola == "") {
            $nazovError = "chyba : Nezadali ste názov.";
            $kod = "1";
        }

        if (strlen($nazovKontrola) > 20 ) {
            $nazovError = "chyba : Názov nesmie obsahovať viac ako 20 znakov";
            $kod = "1";
        }
        if ($hodnotenieKontrola == "") {
            $_POST['hodnotenie'] = 0;
            $hodnotenieKontrola = 0;
        }
        if (is_numeric(trim($hodnotenieKontrola)) == false) {
            $hodnotenieError = "chyba : Uveďte číselnú hodnotu.";
            $kod = "2";
        }
        if ($hodnotenieKontrola > 5 || $hodnotenieKontrola < 0) {
            $hodnotenieError = "chyba: Hodnotenie musí byť v intervale <0, 5>";
            $kod = "2";
        }
        if (strlen($kontaktKontrola) != 13) {
            $kontaktError = "chyba : Číslo musí byť v tvare +421.. a obsahovať 13 znakov.";
            $kod = "3";
        }
        if (!preg_match("/^[+][0-9]{1,3}[0-9]{9}$/", $kontaktKontrola)) {
            $kontaktError = "chyba : Kontakt nie je v správnom tvare.";
            //$kod = "3";
        }
        if ($pscKontrola == "") {
            $pscError = "chyba : Nevyplnili ste psč.";
            $kod = "4";
        }
        if ($ulicaKontrola == "") {
            $ulicaError = "chyba : Nevyplnili ste ulicu.";
            $kod = "5";
        }
        if ($mestoKontrola == "") {
            $mestoError = "chyba : Nevyplnili ste mesto.";
            $kod = "6";
        }
        if ($minutazKontrola == "") {
            $minutazError = "chyba : Nevyplnili ste minutaz.";
            $kod = "7";
        }
        if ($minutazKontrola > 180 || $minutazKontrola < 10) {
            $minutazError = "chyba : Minutaz musí byť v rozmedzí <10; 180>.";
            $kod = "7";
        }
        if (!is_numeric($minutazKontrola)) {
            $minutazError = "chyba : Minutáž musí byť číselná hodnota.";
            $kod = "7";
        }
    }


function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>